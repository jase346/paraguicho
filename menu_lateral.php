<?
    include_once $_SESSION['base_url'].'/class/system.php';
    $system = new System;
    
?>
<div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">
        
            <div class="sidebar-header">
                <div class="sidebar-title">
                    Menú
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">
                        <ul class="nav nav-main">
                            <li class="nav-active">
                            <a href="../">
                              <i class="fa fa-home" aria-hidden="true"></i>
                              <span>Inicio</span>
                            </a>
                            </li>
<? if($_SESSION["nivel"] < 2){ // Menú administrativo?>
                                <li class="nav-parent">
                                <a>
                                  <i class="fa fa-users" aria-hidden="true"></i>
                                  <span>Usuarios</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/admin/vista_usuarios.php' ?>">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                            <span>Estadales</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/admin/vista_usuarios2.php' ?>">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                            <span>Municipales</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                                </li>
                               <li class="nav-parent">
                                <a>
                                  <i class="fa fa-car" aria-hidden="true"></i>
                                  <span>Líneas de Transporte</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/admin/vista_lineas_gen.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de líneas</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            
                            <li class="nav-parent">
                              <a>
                                <i class="fa fa-car" aria-hidden="true"></i>
                                <span>Transportistas</span>
                              </a>
                              <ul class="nav nav-children">
                                <li>
                                  <a href="<?= $_SESSION['base_url1'].'app/admin/transportistas/index.php' ?>">
                                      <i class="fa fa-list" aria-hidden="true"></i>
                                      <span>Listado</span>                                        
                                  </a>
                                </li>
                              </ul>
                            </li>
                            
                            <li class="nav-parent">
                              <a>
                                <i class="fa fa-car" aria-hidden="true"></i>
                                <span>Marcas y Modelos</span>
                              </a>
                              <ul class="nav nav-children">
                                <li>
                                  <a href="<?= $_SESSION['base_url1'].'app/admin/agregar_marcas.php' ?>">
                                      <i class="fa fa-plus" aria-hidden="true"></i>
                                      <span>Agregar Marcas de Vehiculos</span>                                        
                                  </a>
                                </li>
                                <li>
                                  <a href="<?= $_SESSION['base_url1'].'app/admin/agregar_modelos.php' ?>">
                                      <i class="fa fa-plus" aria-hidden="true"></i>
                                      <span>Agregar Modelos de Vehiculos</span>                                        
                                  </a>
                                </li>
                              </ul>
                            </li>

                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                  <span>Rutas</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/admin/vista_rutas.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de rutas</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-mail-forward" aria-hidden="true"></i>
                                  <span>Despacho</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                      <a href="<?= $_SESSION['base_url1'].'app/admin/index.php' ?>">
                                          <i class="fa fa-building" aria-hidden="true"></i>
                                          <span>Proveedurías</span>                                        
                                      </a>
                                    </li>
                                    <li>
                                      <a href="<?= $_SESSION['base_url1'].'app/admin/index.php' ?>">
                                          <i class="fa fa-building" aria-hidden="true"></i>
                                          <span>Tiendas Asociadas</span>                                        
                                      </a>
                                    </li>
                                </ul>
                              </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                  <span>Mensajes</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>Recibidos</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Enviados</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span>Nuevo Mensaje</span>                                        
                                        </a>
                                      </li>
                                 </ul>
                            </li>
                              <li class="nav-parent">
                                <a>
                                  <i class="fa fa-book" aria-hidden="true"></i>
                                  <span>Inventario</span>
                                </a>
                                <ul class="nav nav-children">
                                  <li>
                                    <a href="<?= $_SESSION['base_url1'].'app/admin/inventario/ver_inventario.php' ?>">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                        <span>Ver Inventario</span>                                        
                                    </a>
                                  </li>
                                  <li>
                                    <a href="<?= $_SESSION['base_url1'].'app/admin/inventario/importar_inventario.php' ?>">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                        <span>Importar Inventario</span>                                        
                                    </a>
                                  </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-truck" aria-hidden="true"></i>
                                  <span>Asignaciones</span>
                                </a>
                                <ul class="nav nav-children">
                                  <li>
                                    <a href="<?= $_SESSION['base_url1'].'app/admin/asignaciones/asignaciones.php' ?>">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                        <span>Ver Asignaciones</span>                                        
                                    </a>
                                  </li>
                                  <li>
                                    <a href="<?= $_SESSION['base_url1'].'app/admin/asignaciones/detalle_asignaciones.php' ?>">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                        <span>Ver Detalles</span>                                        
                                    </a>
                                  </li>
                                </ul>
                            </li>
<? } if($_SESSION["nivel"] == 2){ // menú estadal?>
                              <li class="nav-parent">
                                  <a>
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    <span>Usuarios</span>
                                  </a>
                                <ul class="nav nav-children">
                                  <li>
                                    <a href="<?= $_SESSION['base_url1'].'app/estado/vista_usuarios.php' ?>">
                                      <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                      <span>Municipales</span>                                        
                                    </a>  
                                  </li>
                                </ul>
                              </li>
                            <li class="nav-parent">
                              <a>
                                <i class="fa fa-car" aria-hidden="true"></i>
                                <span>Líneas de Transporte</span>
                              </a>
                              <ul class="nav nav-children">
                                <li>
                                  <a href="<?= $_SESSION['base_url1'].'app/estado/vista_lineas.php' ?>">
                                      <i class="fa fa-list" aria-hidden="true"></i>
                                      <span>Listado de líneas</span>                                        
                                  </a>
                                </li>
                              </ul>
                            </li>
                            <li class="nav-parent">
                              <a>
                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                <span>Rutas</span>
                              </a>
                              <ul class="nav nav-children">
                                <li>
                                  <a href="<?= $_SESSION['base_url1'].'app/estado/vista_rutas.php' ?>">
                                      <i class="fa fa-list" aria-hidden="true"></i>
                                      <span>Listado de rutas</span>                                        
                                  </a>
                                </li>
                                <li>
                                  <a href="#">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    <span>Incluir ruta inter estadal</span>                                        
                                  </a>
                                </li>
                              </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-mail-forward" aria-hidden="true"></i>
                                  <span>Despacho</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-building" aria-hidden="true"></i>
                                            <span>Proveedurías</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-building" aria-hidden="true"></i>
                                            <span>Tiendas Asociadas</span>                                        
                                        </a>
                                        </li>
                                 </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                  <span>Mensajes</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>Recibidos</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Enviados</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span>Nuevo Mensaje</span>                                        
                                        </a>
                                      </li>
                                 </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                                  <span>Asignaciones</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Directas</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-bank" aria-hidden="true"></i>
                                            <span>Institucionales</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/asignaciones/estadales.php' ?>">
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                            <span>Hechas al Estado</span>                                        
                                        </a>
                                      </li>
                                 </ul>
                            </li>
<? } if($_SESSION["nivel"] == 3) { // menú municipal?>
                                <li class="nav-parent">
                                <a>
                                  <i class="fa fa-car" aria-hidden="true"></i>
                                  <span>Líneas de Transporte</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/municipio/vista.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de líneas</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/municipio/incluir_linea.php' ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <span>Incluir línea de transporte</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                                </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                  <span>Rutas</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/municipio/vista_rutas_mun.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de rutas</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/municipio/incluir_rutas.php' ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <span>Incluir rutas</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                  <span>Documentos</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'assets/docs/planilla_operadoras.pdf' ?>" target="_blank">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            <span>Planilla de Registro operadoras</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                  <span>Mensajes</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>Recibidos</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Enviados</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span>Nuevo Mensaje</span>                                        
                                        </a>
                                      </li>
                                 </ul>
                            </li>
<? } if($_SESSION["nivel"] ==4) { // menú líneas de transporte?>
                               <li class="nav-parent">
                                <a>
                                  <i class="fa fa-users" aria-hidden="true"></i>
                                  <span>Afiliados</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/ltransporte/incluir_afiliado.php' ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <span>Incluir Afiliado</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/ltransporte/vista.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Lista de afiliados</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                               <li class="nav-parent">
                                <a>
                                  <i class="fa fa-car" aria-hidden="true"></i>
                                  <span>Unidades</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/ltransporte/vista_unidades.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de unidades</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                                </li>
                                <li class="nav-parent">
                                    <a>
                                      <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                      <span>Documentos</span>
                                    </a>
                                    <ul class="nav nav-children">
                                          <li>
                                            <a href="<?= $_SESSION['base_url1'].'assets/docs/planilla_asociados.pdf' ?>" target="_blank">
                                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                <span>Planilla de Registro de Asociados</span>                                        
                                            </a>
                                          </li>
                                      </ul>
                                </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                  <span>Mensajes</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>Recibidos</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Enviados</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="#">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span>Nuevo Mensaje</span>                                        
                                        </a>
                                      </li>
                                 </ul>
                            </li>
<? } if($_SESSION["nivel"] == 5){ // menú afiliados o transportistas?>                                                                
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-user" aria-hidden="true"></i>
                                  <span>Unidades de Transporte</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/incluir_conduc.php' ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <span>Incluir Conductor</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/vista.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de Unidades</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/vista_conductores.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado Conductores</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-users" aria-hidden="true"></i>
                                  <span>Carga Familiar</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/incluir_carga.php?agregar='.base64_encode($_SESSION['user']) ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <span>Incluir Carga Familiar</span>                                        
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/vista_carga.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Carga Familiar</span>                                        
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            <!--menu solicitudes-->
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-copy" aria-hidden="true"></i>
                                  <span>Solicitudes</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/incluir_solicitud.php'?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <span>Crear Solicitud</span>                     
                                        </a>
                                      </li>
                                       <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/usuario/vista_solicitudes.php' ?>">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span>Listado de sus Solicitudes</span>                   
                                        </a>
                                      </li>
                                  </ul>
                            </li>
                            <!--menu solicitudes-->
                            <li class="nav-parent">
                                <a>
                                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                  <span>Mensajes</span>
                                </a>
                                <ul class="nav nav-children">
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>Recibidos</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Enviados</span>                                        
                                        </a>
                                      </li>
                                      <li>
                                        <a href="<?= $_SESSION['base_url1'].'app/estado/index.php' ?>">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span>Nuevo Mensaje</span>                                        
                                        </a>
                                      </li>
                                 </ul>
                            </li>
<? } ?>                                
                        </ul>
                    </nav>
                </div>
            </div>
        </aside>