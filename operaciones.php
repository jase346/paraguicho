<?
	if(!isset($_SESSION))
  	{
    	session_start();
  	}

	include_once $_SESSION['base_url'].'/class/system.php';
	include_once $_SESSION['base_url'].'/helpers/others_functions.php';
	$system = new System;


	switch ($_REQUEST['action']) {

		case 'buscar_persona':
			$system->sql = "SELECT * from rep_sucre where cedula = ".$_GET['ced'];
			echo json_encode($system->sql());
		break;
            
		case 'modelo':
			$system->sql = "SELECT * from modelos_vehiculos where id_marca = ".$_GET['m'];
			echo json_encode($system->sql());
		break;
        
        case 'update_perfil':
				$system->table = 'users';
				$system->where = "id = ".$_POST['id_modificar'];
				unset($_POST['action']);
				unset($_POST['id_modificar']);
                $_POST['password'] = password_hash( $_POST['password'], PASSWORD_DEFAULT );
				$respuesta = $system->modificar($_POST);
				$respuesta['modificar'] = 1;
                $_SESSION['flash'] = 1;
				echo json_encode($respuesta);   

		break;
            
        case 'update_datos_usuario':
				$system->table = 'users';
				$system->where = "id = ".$_POST['id_modificar'];
				unset($_POST['action']);
				unset($_POST['id_modificar']);
				$respuesta = $system->modificar($_POST);
				$respuesta['modificar'] = 1;

			if($respuesta['r'] === true)
			{	
                $_SESSION['nom'] = $_POST['nombre'];
                $_SESSION['ape'] = $_POST['apellido'];
				
			}            
            $_SESSION['flash'] = 1;
				echo json_encode($respuesta);   
                header('location: ../perfil.php');
		break;
            
        case 'change_password_default':
			
			$system->table = "users";
			$arreglo = ['password_activo' => 1,
                        'password' => password_hash( $_POST['password'], PASSWORD_DEFAULT )
                       ];
			$system->where = "id = $_SESSION[user_id]";
			$res = $system->modificar($arreglo);

			if($res['r'] === true)
			{	
				$_SESSION['pass_activo'] = '1';

				
			}
            $_SESSION['flash'] = 1;
			echo json_encode($res);

		break;
            
        case 'grabar':
        $system->table ='unidades';
        $system->where = "placa = '$_POST[placa]'";
        if($system->count() > 0)
        {
            $data = ["r" => false];
            echo json_encode($data);
        }
        else
        {	
            unset($_POST['action']);				
            unset($_POST['id_modificar']);
            $system->table ="unidades";
            echo json_encode($system->guardar($_POST));
        }
		break;
             
        case 'grabar_conduc':
        $system->table ='avances';
        $system->where = "nac_avan = '$_POST[nac_avan]' and ced_avan = '$_POST[ced_avan]'";
        if($system->count() > 0)
        {
            $data = ["r" => false];
            echo json_encode($data);
        }
        elseif(!empty($_POST[id_unidad])) 
        {	
            unset($_POST['action']);				
            unset($_POST['id_modificar']);
            $system->table ="avances";
            $_SESSION['flash'] = 1;
            echo json_encode($system->guardar($_POST));
        }
            else{
            
            unset($_POST['id_unidad']);				
            unset($_POST['action']);				
            unset($_POST['id_modificar']);
            $system->table ="avances";
            $_SESSION['flash'] = 1;
            echo json_encode($system->guardar($_POST));
            }
		break;
  
     		case 'modificar':
				$system->table = 'users';
				$system->where = "id = ".$_POST['id_modificar'];
				unset($_POST['action']);
				unset($_POST['id_modificar']);

				$respuesta = $system->modificar($_POST);
				$respuesta['modificar'] = 1;
				echo json_encode($respuesta);
		break;
 
        case 'modificar_conduc':

                $system->table = 'avances';
				$system->where = "id = ".$_POST['id_modificar'];
				unset($_POST['action']);
				unset($_POST['id_modificar']);
				$respuesta = $system->modificar($_POST);
                $_SESSION['flash'] = 1;
				$respuesta['modificar'] = 1;
				echo json_encode($respuesta);
		break;
        
		case 'remover_linea':
            $system->table = "users";
			$system->eliminar(base64_decode($_GET['eliminar']));
			header('location: ./vista.php');
        
		break; 
        
		case 'remover_conduc':
            $system->table = "avances";
			$system->eliminar(base64_decode($_GET['eliminar']));
			header('location: ./vista_conductores.php');
        
		break; 

		/**********************************
		Controladores de las solicitudes
		***********************************/
		case 'validar_item':
			$system->sql = "select * from rubros where id=$_GET[id_rubro]";
			$item = $system->sql();
			if (count($item)>0) {
				$id_user = base64_decode($_GET['id_user']);
				$fechaInicio=calculaFecha("days",$item[0]->dias_no_habil*-1);
				$system->sql = "select a.*,b.* from solicitudes as a
					inner join detalles_solicitudes as b on (a.id=b.id_solicitud)
					where a.id_user=$id_user
					and estado=$_GET[estado]
					and municipio=$_GET[municipio] 
					and b.id_unidad=$_GET[id_unidad]
					and b.id_rubro=$_GET[id_rubro]
					and (a.estatus <> 3 and b.estatus<>4)
					and a.fec_solicitud > '$fechaInicio'";
				$items = $system->sql();
				if (count($items)>0) {
					$sum = 0;
					foreach ($items as $val) {
						$sum += $val->cantidad;
					}
					unset($items);
					$sum2=$sum+$_GET['cant_sol'];
					if ($sum >= $_GET['cant_max']) {
						echo json_encode(array('msg'=>'No puede solicitar este item.', 'r'=>true,'case'=>3));
					}elseif ($sum2 <= $_GET['cant_max']) {
						echo json_encode(array('msg' => 'Item, Agregado a la lista de detalles.','r'=>true, 'case' => 1));
					}else{
						$disp = $_GET['cant_max'] - $sum;
						echo json_encode(array('msg'=>'Solo tiene disponible '.$disp.' '.$item[0]->descripcion.' para solicitar.','r'=>true,'case'=>2,'dis'=>$disp));
					}
				}else{
					echo json_encode(array('msg' => 'Item, Agregado a la lista de detalles.','r'=>true, 'case' => 1));
				}
			}else{
				echo json_encode(array('msg' => 'Error, no se encontro relacion con ningun item dentro de la BD.','r'=>false));
			}
		break;

		case 'guardar_solicitud':
			//armamos solicitud
		$id_user = base64_decode($_GET['id_user']);
			$sol = array(
				'id_user' => $id_user,
				'cod_linea' => $_GET['cod_linea'],
				'cod_afiliado' => $_GET['cod_afiliado'],
				'estado' => $_GET['estado'],
				'municipio' => $_GET['municipio'],
				'fec_solicitud' => date('Y-m-d')
			);
			$system->table ='solicitudes';
            $res=$system->guardar($sol);
            if ($res['r']) {
            	unset($sol);
            	$dia=date('Y-m-d');
            	$system->sql="select id from solicitudes where id_user=$id_user and fec_solicitud='$dia'";
            	unset($dia);
            	$solicitud = $system->sql();
            	if (count($solicitud) > 0) {
            		$tabla = $_GET['detalles'];
            		$fin = count($tabla);
            		$system->table ='detalles_solicitudes';
            		for ($i=0; $i < $fin; $i++) {
            			$arr = array(
            				'id_user' => $id_user,
            				'id_solicitud' => $solicitud[0]->id,
            				'id_unidad' => $tabla[$i][0],
            				'id_rubro' => $tabla[$i][1],
            				'cantidad' => $tabla[$i][2]
            			);
            			$res=$system->guardar_multiple($arr);
            		}
            		unset($tabla);
            		unset($id_user);
            		unset($fin);
            		if ($res) {
            			echo json_encode(array('msg'=>'Solicitud Registrada', 'r'=>true));
            		}else{
            			echo json_encode(array('msg'=>'Error al registrar los detalles de la solicitud', 'r'=>true));
            		}
            	}else{
            		echo json_encode(array('msg'=>'Error BD al encontrar id de la solicitud registrada', 'r'=>false));
            	}
            }else{
            	echo json_encode(array('msg'=>'Error BD al registrar la solicitud', 'r'=>false));
            }
            //echo json_encode(array('msg'=>'Error BD al registrar la solicitud', 'r'=>false));
		break;

		case 'mostrar_Items':
			$system->sql="select * from solicitudes as a 
inner join detalles_solicitudes as b on (a.id=b.id_solicitud) 
where a.id=$_GET[id]";
			echo json_encode($system->sql());
		break;
            
		default:
			# code...
			break;
	}
?>